FROM maven:3-openjdk-11 AS tycho

COPY tycho /tmp/tycho
COPY .git /tmp/.git
WORKDIR /tmp/tycho
RUN mvn -B package

FROM openjdk:11 AS runtime

RUN useradd user
USER user
WORKDIR /home/user/

# Copy in the Tycho-built server product
COPY --chown=user:user --from=tycho /tmp/tycho/releng/uk.ac.aston.provlayer.cdo.server.product/target/products/uk.ac.aston.provlayer.cdo.server.product/linux/gtk/x86_64/cdo-server/ cdo-server/

# Create an empty folder owned by this user for the DB
RUN mkdir /home/user/cdo-server/h2

# Add the custom entrypoint
USER root
COPY docker/entrypoint.sh /usr/local/bin/entrypoint.sh
USER user

WORKDIR /home/user/cdo-server/
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD ["/home/user/cdo-server/eclipse"]
